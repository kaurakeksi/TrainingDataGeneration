﻿Shader "Custom/TileShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_FaceTex("Face texture (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Margin ("Margin", Range(0, 0.2)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0


		sampler2D _MainTex;
		sampler2D _FaceTex;

		struct Input {
			float2 uv_MainTex;
		};

		
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		half _Margin;
		float4 _FaceTex_TexelSize;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		float4 aa(float2 uv, fixed4 origC)
		{
			float4 result = float4(0, 0, 0, 0);
			int s = 5;
			float size = s;
			int loopcount = 0;

			int goodValueCount = 0;
			for (int i = 0; i < s; i++)
			{
				for (int j = 0; j < s; j++)
				{
					float coordx = uv.x + (i - (s - 1) / 2.0) * _FaceTex_TexelSize.x;
					float coordy = uv.y + (j - (s - 1) / 2.0) * _FaceTex_TexelSize.y;
					if (coordx < 0.0f)
					{
						coordx = coordx + 1.0f;
					}
					else if (coordx > 1.0f)
					{
						coordx = coordx - 1.0f;
					}
					if (coordy < 0.0f)
					{
						coordy = coordy + 1.0f;
					}
					else if (coordy > 1.0f)
					{
						coordy = coordy - 1.0f;
					}
					fixed4 temp = tex2D(_FaceTex, float2(coordx, coordy));

					if (temp.a > 0.01)
					{
						goodValueCount++;
						result.rgb = result.rgb + temp.rgb;
						result.a = result.a + temp.a;
					}
					loopcount++;
				}
			}
			float count = goodValueCount;
			result.rgb = result.rgb / count;
			result.a = result.a / count;
			if (goodValueCount < 1)
			{
				result = origC;//tex2D(_FaceTex, uv);
				//result.a = 0;
			}
			return result;
		}

		void surf (Input IN, inout SurfaceOutputStandard o)
		{
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

			float ratio = 0.7353515625;
			if (IN.uv_MainTex.x <= ratio)
			{
				float xCoord = IN.uv_MainTex.x / ratio;
				float yCoord = IN.uv_MainTex.y;

				xCoord = xCoord * (1 + 2 * _Margin) - _Margin;
				yCoord = yCoord * (1 + 2 * _Margin) - _Margin;
				float2 uv_FaceTex = float2(xCoord, yCoord);

				if (uv_FaceTex.x >= 0.0 && uv_FaceTex.x <= 1.0 && uv_FaceTex.y >= 0.0 && uv_FaceTex.y <= 1.0)
				{
					fixed4 f = tex2D(_FaceTex, uv_FaceTex);
					//if (f.a > 0.01)
					{
						f = aa(uv_FaceTex, c);
						c = f; 
					}
				}
			}
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
