﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEngine.Rendering.PostProcessing;

public class RectData
{
    public Rect Rect;
    public string ClassName;
    public RectData(Rect rect, string className)
    {
        Rect = rect;
        ClassName = className;
    }
}

public class TrainDataGenerator : MonoBehaviour
{
    [SerializeField]
    private Camera _camera;
    
    [SerializeField]
    private float _cameraMaxXZDistance;

    [SerializeField]
    private Transform _tableTopPosition;

    [SerializeField]
    private Transform _tableMiddleBox;

    [SerializeField]
    private int _scenarioCount;

    [SerializeField]
    private int _startFromScenario;

    [SerializeField]
    private GameObject _tilePrefab;

    [SerializeField]
    private GameObject _faceDownPrefab;

    [SerializeField]
    private int _tileCount;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _faceDownChance;

    [SerializeField]
    private List<Material> _skyboxes;

    [SerializeField]
    private Texture _rectDebugTexture;

    [SerializeField]
    private float _scenarioCooldown;

    [SerializeField]
    private Light _light;

    [SerializeField]
    private PostProcessVolume _postVolume;

    [SerializeField]
    private float _minGrainIntensity;

    [SerializeField]
    private float _maxGrainIntensity;

    [SerializeField]
    private float _minGrainSize;

    [SerializeField]
    private float _maxGrainSize;

    [SerializeField]
    private float _minGrainLuminanceContribution;

    [SerializeField]
    private float _maxGrainLuminanceContribution;

    [SerializeField]
    private Color _minLightColor;

    [SerializeField]
    private Color _maxLightColor;

    [SerializeField]
    private List<GameObject> _shadowObjects;

    [Range(0.0f, 1.0f)]
    [SerializeField]
    private float _artificialShadowChance;

    [SerializeField]
    private List<Material> _tileMaterials;

    [SerializeField]
    private GameObject _imagePlane;

    [SerializeField]
    private string _imagePlaneFilePath = "D:\\data\\VOCdevkit\\VOC2007\\JPEGImages";

    [SerializeField]
    private float _maxGlobalTileOffset;

    private float _cameraDistanceFromTableTop;
    private Collider _tableTopAreaCollider;
    private Collider _tableMiddleBoxCollider;

    private List<GameObject> _tiles;
    private List<RectData> _rects;
    private int _scenariosDone;
    private string _date;
    private float _previousScenarioTime;
    private bool _dataGenerationDone;
    private float _minXLightRotation;

    private int _savedScenarios;
    private float _previousSaveTime;

    private Grain _grainLayer;
    private AmbientOcclusion _aoLayer;
    private ColorGrading _gradingLayer;
    private Blur _blur;
    private Bloom _bloom;

    private Material _imagePlaneMaterial;

    private bool _isFirstScenario = true;

    private List<string> _imagePlaneFilenames;
    private int _imagePlaneFilenameIndex;

    private void Awake()
    {
        _cameraDistanceFromTableTop = Vector3.Distance(_camera.transform.position, _tableTopPosition.position);
        _tableTopAreaCollider = _tableTopPosition.GetComponent<Collider>();
        _tableMiddleBoxCollider = _tableMiddleBox.GetComponent<Collider>();

        _tiles = new List<GameObject>();
        _rects = new List<RectData>();

        _date = System.DateTime.Now.ToLongTimeString().Replace(':', '.').Replace(" ", "");
        _previousScenarioTime = Time.time;
        _minXLightRotation = _light.transform.rotation.eulerAngles.x;

        _postVolume.profile.TryGetSettings(out _aoLayer);
        _postVolume.profile.TryGetSettings(out _grainLayer);
        _postVolume.profile.TryGetSettings(out _gradingLayer);
        _postVolume.profile.TryGetSettings(out _blur);
        _postVolume.profile.TryGetSettings(out _bloom);

        _imagePlaneMaterial = _imagePlane.GetComponent<Renderer>().material;
        _imagePlaneFilenames = new List<string>();

        // Load imagePlane filenames
        DirectoryInfo d = new DirectoryInfo(@_imagePlaneFilePath);
        FileInfo[] Files = d.GetFiles("*.jpg");
        foreach (FileInfo file in Files)
        {
            _imagePlaneFilenames.Add(file.Name);
        }
        _imagePlaneFilenameIndex = Random.Range(0, _imagePlaneFilenames.Count);
    }

	private void Update()
    {
        if (_isFirstScenario && _scenariosDone == 0 && _startFromScenario != 0)
        {
            _scenariosDone = _startFromScenario;
            _savedScenarios = _startFromScenario;
            _isFirstScenario = false;
        }
        if (!_dataGenerationDone)
        {
            if (Time.time > _previousScenarioTime + _scenarioCooldown && _scenariosDone > 0 && _savedScenarios < _scenariosDone)
            {
                SaveScenario();
                _previousScenarioTime = Time.time;
                ++_savedScenarios;

                if (_scenariosDone == _scenarioCount)
                {
                    _dataGenerationDone = true;
                }
            }

            if (_scenariosDone < _scenarioCount && Time.time > _previousScenarioTime + _scenarioCooldown)
            {
                // Choose skybox
                if (_skyboxes.Count > 0)
                {
                    RenderSettings.skybox = _skyboxes[Random.Range(0, _skyboxes.Count)];
                }

                UpdateImagePlane();

                // Randomize table hue
                //_tableTopPosition.transform.parent.gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0.6f, 1.0f), Random.Range(0.6f, 1.0f), Random.Range(0.6f, 1.0f), 1.0f);

                // Rotate light
                var euler = _light.transform.rotation.eulerAngles;
                _light.transform.rotation = Quaternion.Euler(Random.Range(_minXLightRotation, 70), euler.y, euler.z);
                if (Random.value < 0.2f)
                {
                    _light.intensity = Random.Range(0.35f, 1.3f);
                }
                else
                {
                    _light.intensity = Random.Range(0.35f, 0.8f);
                }
                _light.color = Color.Lerp(_minLightColor, _maxLightColor, Random.value);
                _light.shadowStrength = Random.Range(0.5f, 0.65f);

                // Place camera
                float radius = Random.Range(0, _cameraMaxXZDistance);
                float xPos = Mathf.Cos(Random.Range(0, 2 * Mathf.PI)) * radius;
                float zPos = Mathf.Sin(Random.Range(0, 2 * Mathf.PI)) * radius;
                float b = Random.Range(_cameraDistanceFromTableTop * 0.5f, _cameraDistanceFromTableTop * 0.7f);
                _camera.transform.position = new Vector3(_camera.transform.position.x, b, _camera.transform.position.z);
                _camera.transform.position = new Vector3(xPos, Mathf.Sqrt(b * b - xPos * xPos - zPos * zPos), zPos) + _tableMiddleBox.position;
                _camera.transform.rotation = Quaternion.LookRotation((_tableMiddleBox.position - _camera.transform.position).normalized);

                // Artificial shadow casting
                if (Random.value < _artificialShadowChance)
                {
                    foreach (var obj in _shadowObjects)
                    {
                        obj.SetActive(true);
                    }
                }
                else
                {
                    foreach (var obj in _shadowObjects)
                    {
                        obj.SetActive(false);
                    }
                }

                // Randomize post processing stack
                _grainLayer.intensity.value = Random.Range(_minGrainIntensity, _maxGrainIntensity);
                _grainLayer.size.value = Random.Range(_minGrainSize, _maxGrainSize);
                _grainLayer.lumContrib.value = Random.Range(_minGrainLuminanceContribution, _maxGrainLuminanceContribution);

                if (Random.value > 0.5)
                {
                    _grainLayer.active = false;
                }
                else
                {
                    _grainLayer.active = true;
                }

                _gradingLayer.gain.value = new Vector4(1, 1, 1, Random.Range(-0.1f, 0.1f));
                _gradingLayer.contrast.value = Random.Range(-20.0f, -5.0f);

                _gradingLayer.temperature.value = Random.Range(-10.0f, 10.0f);
                _bloom.active = Random.value > 0.5f;
                _bloom.intensity.value = Random.Range(4.0f, 8.0f);


                if (Random.value > 0.5)
                {
                    _aoLayer.active = true;
                }
                else
                {
                    _aoLayer.active = false;
                }

                _blur.enabled.value = Random.value > 0.8f;
                _blur.BlurSize.value = Random.value * 0.05f;

                // Remove previous scenario's tiles
                foreach (var tile in _tiles)
                {
                    Destroy(tile);
                }
                _tiles.Clear();
                _rects.Clear();

                // Place tiles
                List<GameObject> tileObjects = new List<GameObject>();
                Vector3 globalTileOffset = new Vector3(Random.value, 0, Random.value) * _maxGlobalTileOffset;
                tileObjects = GetTiles(globalTileOffset);
                //tileObjects = GetCallTiles();
                Rect rect;

                int materialRandom = Random.Range(0, _tileMaterials.Count);
                _faceDownPrefab.GetComponentInChildren<Renderer>().material = _tileMaterials[materialRandom];
                _tilePrefab.GetComponent<Renderer>().material = _tileMaterials[materialRandom];

                for (int k = tileObjects.Count - 1; k >= 0; --k)
                {
                    var tileScript = tileObjects[k].GetComponent<Tile>();
                    if (GUIRectWithTile(tileObjects[k], out rect))
                    {
                        var scrpt = tileObjects[k].GetComponent<Tile>();
                        if (scrpt == null)
                        {
                            scrpt = tileObjects[k].GetComponentInChildren<Tile>();
                        }
                        _tiles.Add(tileObjects[k]);
                        if (scrpt.Initialized)
                        {
                            _rects.Add(new RectData(rect, (tileScript != null) ? tileScript.Value.ToString() : TileValue.FaceDown.ToString()));
                        }
                    }
                    else
                    {
                        Destroy(tileObjects[k]);
                        tileObjects.RemoveAt(k);
                    }
                }

                ++_scenariosDone;
                _previousScenarioTime = Time.time;
            }
        }
        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();
    }

    private void UpdateImagePlane()
    {
        Texture2D tex = LoadJPG(_imagePlaneFilePath + "\\" + _imagePlaneFilenames[_imagePlaneFilenameIndex]);
        _imagePlaneMaterial.mainTexture = tex;
        ++_imagePlaneFilenameIndex;
        if (_imagePlaneFilenameIndex >= _imagePlaneFilenames.Count)
        {
            _imagePlaneFilenameIndex = 0;
        }
    }

    public Texture2D LoadJPG(string filePath)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }

    private void SaveScenario()
    {
        // Create output directory
        string path = Directory.GetCurrentDirectory() + "\\Output\\" + _date + "\\";
        string annotationsPath = path + "Annotations\\";
        string imagesPath = path + "Images\\";
        if (!Directory.Exists(annotationsPath))
        {
            Directory.CreateDirectory(annotationsPath);
        }
        if (!Directory.Exists(imagesPath))
        {
            Directory.CreateDirectory(imagesPath);
        }

        string filename = _scenariosDone.ToString().PadLeft(6, '0');
        // Write screenshot to file
        ScreenCapture.CaptureScreenshot(imagesPath + filename + ".png", 1);
        string txtFilePath = annotationsPath + filename + ".txt";

        //var texss = ScreenCapture.CaptureScreenshotAsTexture(1);
        //var bytes = ImageConversion.EncodeToJPG(texss);
        //System.IO.File.WriteAllBytes(imagesPath + filename + "_1.png", bytes);

        // Write tile info to file
        string rectinfo = "";
        foreach (var obj in _rects)
        {
            // xmin, ymin, xmax, ymax
            rectinfo += obj.ClassName + "," + Mathf.RoundToInt(obj.Rect.x) + "," + Mathf.RoundToInt(obj.Rect.y) + "," + Mathf.RoundToInt(obj.Rect.x + obj.Rect.size.x) + "," + Mathf.RoundToInt(obj.Rect.y + obj.Rect.size.y) + System.Environment.NewLine;
        }
        File.WriteAllText(txtFilePath, rectinfo);
    }

    private List<GameObject> GetTiles(Vector3 globalTileOffset)
    {
        List<GameObject> tiles = new List<GameObject>();
        AddPoolTiles(tiles, Random.Range(1, 18), globalTileOffset);
        AddWallTiles(tiles, globalTileOffset);
        return tiles;
    }
    
    private List<GameObject> GetCallTiles()
    {
        List<GameObject> tiles = new List<GameObject>();

        GameObject tempTile = Instantiate(_tilePrefab, Vector3.zero, Quaternion.identity);
        var tileInformation = tempTile.GetComponent<BoxCollider>();
        Vector3 pos = Vector3.zero;

        float minx = _tableTopAreaCollider.bounds.min.x;
        float maxx = _tableTopAreaCollider.bounds.max.x;
        float minz = _tableTopAreaCollider.bounds.min.z;
        float maxz = _tableTopAreaCollider.bounds.max.z;

        List<GameObject> tempTileList = new List<GameObject>();
        for (int i = 0; i < 50; ++i)
        {
            int materialRandom = Random.Range(0, _tileMaterials.Count);
            _faceDownPrefab.GetComponentInChildren<Renderer>().material = _tileMaterials[materialRandom];
            _tilePrefab.GetComponent<Renderer>().material = _tileMaterials[materialRandom];
            
            float maxXDisplacement = 0.05f * tileInformation.size.x;
            float maxZDisplacement = 0.05f * tileInformation.size.z;
            float setWidth = 0.0f;
            float setHeight = tileInformation.size.z;
            int randomValue = Random.Range(0, 3);
            switch (randomValue)
            {
                // Pon
                case 0:
                    Vector3 tempPos = Vector3.zero;
                    int turnedTile = Random.Range(0, 2);
                    for (int k = 0; k < 3; ++k)
                    {
                        GameObject tile = Instantiate(_tilePrefab, (turnedTile == k) ? tempPos - new Vector3(-0.5f * (tileInformation.size.z - tileInformation.size.x), 0, 0.5f * (tileInformation.size.z - tileInformation.size.x)) : tempPos,
                            Quaternion.Euler(new Vector3(0, (turnedTile  == k) ? 90 : 0, 0)));
                        tile.GetComponent<Tile>().InitializeTile(false);
                        tempTileList.Add(tile);

                        if (turnedTile == k)
                        {
                            tempPos += new Vector3(tileInformation.size.z + Random.Range(0.0f, maxXDisplacement), 0, Random.Range(0.0f, maxZDisplacement));
                            setWidth += tileInformation.size.z;
                        }
                        else
                        {
                            tempPos += new Vector3(tileInformation.size.x + Random.Range(0.0f, maxXDisplacement), 0, Random.Range(0.0f, maxZDisplacement));
                            setWidth += tileInformation.size.x;
                        }
                    }
                    break;
                // Open kan
                case 1:
                    Vector3 tempPos2 = Vector3.zero;
                    int turnedTile2 = Random.Range(0, 3);
                    for (int k = 0; k < 3; ++k)
                    {
                        GameObject tile = Instantiate(_tilePrefab, (turnedTile2 == k) ? tempPos2 - new Vector3(-0.5f * (tileInformation.size.z - tileInformation.size.x), 0, 0.5f * (tileInformation.size.z - tileInformation.size.x)) : tempPos2,
                            Quaternion.Euler(new Vector3(0, (turnedTile2 == k) ? 90 : 0, 0)));
                        tile.GetComponent<Tile>().InitializeTile(false);
                        tempTileList.Add(tile);

                        if (turnedTile2 == k)
                        {
                            GameObject tile2 = Instantiate(_tilePrefab, (turnedTile2 == k) ? tempPos2 - new Vector3(-0.5f * (tileInformation.size.z - tileInformation.size.x), 0, 0.5f * (tileInformation.size.z - tileInformation.size.x) - tileInformation.size.x) : tempPos2,
                                Quaternion.Euler(new Vector3(0, (turnedTile2 == k) ? 90 : 0, 0)));
                            tile2.GetComponent<Tile>().InitializeTile(false);
                            tempTileList.Add(tile2);
                            setHeight = tileInformation.size.x * 2.0f;

                            tempPos2 += new Vector3(tileInformation.size.z + Random.Range(0.0f, maxXDisplacement), 0, Random.Range(0.0f, maxZDisplacement));
                            setWidth += tileInformation.size.z;
                        }
                        else
                        {
                            tempPos2 += new Vector3(tileInformation.size.x + Random.Range(0.0f, maxXDisplacement), 0, Random.Range(0.0f, maxZDisplacement));
                            setWidth += tileInformation.size.x;
                        }
                    }

                    break;
                // Closed kan
                case 2:
                    Vector3 tempPos3 = Vector3.zero;
                    for (int k = 0; k < 4; ++k)
                    {
                        bool faceDown = (k == 0 || k == 3);
                        GameObject tile = Instantiate(_tilePrefab, faceDown ? tempPos3 + new Vector3(0, tileInformation.size.y, 0) : tempPos3, faceDown ? Quaternion.Euler(0, 0, 180) : Quaternion.identity);
                        if (faceDown)
                        {
                            tile.GetComponentInChildren<Tile>().InitializeTile(true);
                        }
                        else
                        {
                            tile.GetComponent<Tile>().InitializeTile(false);
                        }
                        tempTileList.Add(tile);

                        tempPos3 += new Vector3(tileInformation.size.x + Random.Range(0.0f, maxXDisplacement), 0, Random.Range(0.0f, maxZDisplacement));
                        setWidth += tileInformation.size.x;
                    }
                    break;
                default:
                    break;
            }

            // Have 10 tries to place the set on table
            bool validPosition = true;
            Vector3 randomPos = Vector2.zero;
            Vector3 randomRotation = new Vector3(0, Random.Range(0, 360), 0);
            for (int k = 0; k < 10; ++k)
            {
                validPosition = true;
                float x = Random.Range(minx + setWidth, maxx - setWidth);
                float z = Random.Range(minz + setHeight, maxz - setHeight);
                randomPos = new Vector3(x, _tableTopPosition.position.y, z);
                for (int j = 0; j < tempTileList.Count; ++j)
                {
                    var tileCopy = Instantiate(tempTileList[j], tempTileList[j].transform.position + randomPos, Quaternion.identity);
                    tileCopy.transform.RotateAround(tempTileList[0].transform.position + randomPos, Vector3.up, randomRotation.y);
                    var tileCollider = tileCopy.GetComponent<BoxCollider>();
                    float maxSize = Mathf.Max(Mathf.Max(tileCollider.size.x,tileCollider.size.y), tileCollider.size.z);
                    tileCollider.size = new Vector3(maxSize, maxSize, maxSize) * Mathf.Sqrt(2);
                    if (_tableMiddleBoxCollider.bounds.Intersects(tileCollider.bounds))
                    {
                        validPosition = false;
                    }
                    foreach (var other in tiles)
                    {
                        var otherCollider = other.GetComponent<Collider>();
                        if (otherCollider.bounds.Intersects(tileCollider.bounds))
                        {
                            validPosition = false;
                        }
                    }
                    Destroy(tileCopy);
                    if (!validPosition)
                    {
                        break;
                    }
                }
                if (validPosition)
                {
                    break;
                }
            }

            if (validPosition)
            {
                for (int k = 0; k < tempTileList.Count; ++k)
                {
                    tempTileList[k].transform.position += randomPos;
                    tempTileList[k].transform.RotateAround(tempTileList[0].transform.position, Vector3.up, randomRotation.y);
                    tiles.Add(tempTileList[k]);
                }
            }
            else
            {
                for (int k = tempTileList.Count - 1; k >= 0; --k)
                {
                    Destroy(tempTileList[k]);
                }
            }
            tempTileList.Clear();
        }
        Destroy(tempTile);
        return tiles;
    }

    private void AddWallTiles(List<GameObject> tiles, Vector3 globalTileOffset)
    {
        GameObject tempTile = Instantiate(_tilePrefab, Vector3.zero, Quaternion.identity);
        var tileInformation = tempTile.GetComponent<BoxCollider>();
        Vector3 pos = Vector3.zero;

        float minx = _tableTopAreaCollider.bounds.min.x;
        float maxx = _tableTopAreaCollider.bounds.max.x;
        float minz = _tableTopAreaCollider.bounds.min.z;
        float maxz = _tableTopAreaCollider.bounds.max.z;
        
        float minZDisplacement = -0.025f * tileInformation.size.z;
        float maxZDisplacement = 0.025f * tileInformation.size.x;

        for (int k = 0; k < 4; ++k)
        {
            pos = _tableTopAreaCollider.transform.position;
            pos = new Vector3((maxx - minx) * 0.5f + minx - tileInformation.size.x * 17.0f * 0.5f, pos.y, maxz - (maxz - minz) * 0.07f);

            Vector3 randomRotation = new Vector3(0, Random.Range(-1.0f, 5.0f), 0);
            for (int i = 0; i < 17; ++i)
            {
                bool faceDown = Random.value > 0.3f;

                float zOffset = i * tileInformation.size.x * Mathf.Sin(-randomRotation.y * Mathf.Deg2Rad);
                GameObject tile = Instantiate(_faceDownPrefab, new Vector3(pos.x + i * tileInformation.size.x, pos.y + tileInformation.size.y, pos.z + zOffset), Quaternion.Euler(randomRotation));
                GameObject tile2 = Instantiate(faceDown ? _faceDownPrefab : _tilePrefab, new Vector3(pos.x + i * tileInformation.size.x, faceDown ? pos.y + tileInformation.size.y * 2.0f : pos.y + tileInformation.size.y * 2.0f, pos.z + zOffset), Quaternion.Euler(randomRotation));

                float zRandomOffset = Random.Range(minZDisplacement, maxZDisplacement);
                tile.transform.Translate(new Vector3(0, 0, zRandomOffset), Space.Self);
                tile2.transform.Translate(new Vector3(0, 0, zRandomOffset), Space.Self);

                tile.transform.RotateAround(_tableMiddleBox.position, Vector3.up, 90 * k);
                tile2.transform.RotateAround(_tableMiddleBox.position, Vector3.up, 90 * k);

                tile.transform.position += globalTileOffset;
                tile2.transform.position += globalTileOffset;

                if (!faceDown)
                {
                    tile2.GetComponent<Tile>().InitializeTile(false);
                }
                else
                {
                    tile2.GetComponentInChildren<Tile>().InitializeTile(true);
                }

                Rect rect;
                if (GUIRectWithTile(tile2, out rect))
                {
                    tiles.Add(tile);
                    tiles.Add(tile2);
                }
                else
                {
                    Destroy(tile);
                    Destroy(tile2);
                }
            }
        }

        Destroy(tempTile);
    }

    private void AddPoolTiles(List<GameObject> tiles, int countPerPool, Vector3 globalTileOffset)
    {
        List<GameObject> poolTileList = new List<GameObject>();
        GameObject tempTile = Instantiate(_tilePrefab, Vector3.zero, Quaternion.identity);
        Vector3 pos = Vector3.zero;

        float minx = _tableTopAreaCollider.bounds.min.x;
        float maxx = _tableTopAreaCollider.bounds.max.x;
        float minz = _tableTopAreaCollider.bounds.min.z;
        float maxz = _tableTopAreaCollider.bounds.max.z;
        
        var tileInformation = tempTile.GetComponent<BoxCollider>();
        for (int k = 0; k < 4; ++k)
        {
            pos = _tableTopAreaCollider.transform.position;
            pos = new Vector3((maxx - minx) * 0.5f + minx, pos.y, maxz - (maxz - minz) * 0.28f);
            pos += new Vector3(tileInformation.size.x * 5 * 0.5f, 0, tileInformation.size.z * 3 * 0.5f);

            int count = 0;
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 6; ++j)
                {

                    Vector3 posOffset = new Vector3(-tileInformation.size.x * j, 0, -tileInformation.size.z * (2 - i));
                    GameObject tile = Instantiate(_tilePrefab, pos + posOffset, Quaternion.identity);

                    tile.transform.RotateAround(_tableMiddleBox.position, Vector3.up, 90 * k);
                    tile.GetComponent<Tile>().InitializeTile(false);
                    poolTileList.Add(tile);
                    ++count;

                    if (countPerPool <= count)
                    {
                        break;
                    }
                }

                if (countPerPool <= count)
                {
                    break;
                }
            }
            for (int i = 0; i < poolTileList.Count; ++i)
            {
                ApplyRandomDisplacement(i, poolTileList, tileInformation.size);
            }
            foreach (GameObject tile in poolTileList)
            {
                tile.transform.Rotate(Random.value > 0.5f ? Vector3.zero : new Vector3(0, 180, 0));
                tile.transform.position += globalTileOffset;
                tiles.Add(tile);
            }
            poolTileList.Clear();
        }

        Destroy(tempTile);
    }

    private void ApplyRandomDisplacement(int index, List<GameObject> poolTileList, Vector3 tileSize)
    {
        //Vector3 randomRotation = new Vector3(0, Random.Range(0, 3), 0);
        //poolTileList[index].transform.Rotate(randomRotation);

        Vector3 randomOffset = Vector3.zero;

        float minXDisplacement = -0.05f * tileSize.x;
        float minZDisplacement = -0.025f * tileSize.z;
        float maxXDisplacement = 0.05f * tileSize.x;
        float maxZDisplacement = 0.025f * tileSize.z;

        // No Z displacement
        if (Random.value > 0.25f)
        {
            minZDisplacement = 0;
            maxZDisplacement = 0;
        }
        // No X displacement
        if (Random.value > 0.5f)
        {
            minXDisplacement = 0;
            maxXDisplacement = 0;
        }

        // If not first row, disable upwards
        if (index > 5)
        {
            minZDisplacement = 0.0f;
        }
        // If not first tile of the row, disable leftwards
        if (index % 6 != 0)
        {
            maxXDisplacement = 0.0f;
        }
        randomOffset = new Vector3(Random.Range(minXDisplacement, maxXDisplacement), 0, Random.Range(minZDisplacement, maxZDisplacement));
        
        FixPool(poolTileList, index, index + 5 - index % 6, randomOffset.x, 0);
        if (randomOffset.z > 0.0f)
        {
            FixPool(poolTileList, index, poolTileList.Count - 1, 0, randomOffset.z);
        }
    }

    private void FixPool(List<GameObject> poolTileList, int startIndex, int endIndex, float deltaX, float deltaZ)
    {
        if (endIndex > poolTileList.Count - 1)
        {
            endIndex = poolTileList.Count - 1;
        }
        for (int i = startIndex; i <= endIndex; ++i)
        {
            poolTileList[i].transform.Translate(new Vector3(deltaX, 0, deltaZ), Space.Self);
        }
    }

    private GameObject GetRandomlyPlacedTile()
    {
        // Init tile object
        bool faceDown = (Random.value < _faceDownChance) ? true : false;

        _faceDownPrefab.GetComponentInChildren<Renderer>().material = _tileMaterials[Random.Range(0, _tileMaterials.Count)];
        _tilePrefab.GetComponent<Renderer>().material = _tileMaterials[Random.Range(0, _tileMaterials.Count)];

        GameObject tile = Instantiate(faceDown? _faceDownPrefab : _tilePrefab, Vector3.zero, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));

        bool validPosition = false;
        Vector3 pos = Vector3.zero;
        while (!validPosition)
        {
            pos = GetRandomPositionOnTable(tile);
            tile.transform.position = pos;

            validPosition = true;
            var tileCollider = tile.GetComponent<Collider>();
            // Check if collides with other tiles
            foreach (var other in _tiles)
            {
                var otherCollider = other.GetComponent<Collider>();
                if (otherCollider.bounds.Intersects(tileCollider.bounds))
                {
                    validPosition = false;
                }
            }
        }

        if (!faceDown)
        {
            tile.GetComponent<Tile>().InitializeTile(false);
        }
        else
        {
            tile.GetComponentInChildren<Tile>().InitializeTile(true);
        }
        return tile;
    }

    private Vector3 GetRandomPositionOnTable(GameObject tile)
    {
        var copy = Instantiate(tile);

        float minx = _tableTopAreaCollider.bounds.min.x;
        float maxx = _tableTopAreaCollider.bounds.max.x;
        float minz = _tableTopAreaCollider.bounds.min.z;
        float maxz = _tableTopAreaCollider.bounds.max.z;

        Vector3 result = Vector3.zero;
        bool validPosition = false;
        while(!validPosition)
        {
            float x = Random.Range(minx, maxx);
            float z = Random.Range(minz, maxz);
            result = new Vector3(x, _tableTopPosition.position.y, z);
            copy.transform.position = result;
            if (!_tableMiddleBoxCollider.bounds.Intersects(copy.GetComponent<Collider>().bounds))
            {
                validPosition = true;
            }
        }
        Destroy(copy);
        return result;
    }

    public bool GUIRectWithTile(GameObject go, out Rect rect)
    {
        var tile = go.GetComponent<Tile>();

        if (tile == null)
        {
            tile = go.GetComponentInChildren<Tile>();
        }

        var col = go.GetComponent<BoxCollider>();
        Vector3 cen = go.transform.TransformPoint(col.center);
        var ext = col.size / 2.0f;

        Vector2[] extentPoints = new Vector2[4]
        {
             //_camera.WorldToScreenPoint(RotatePointAroundPivot(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z-ext.z), cen, go.transform.rotation)),
             //_camera.WorldToScreenPoint(RotatePointAroundPivot(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z-ext.z), cen, go.transform.rotation)),
             //_camera.WorldToScreenPoint(RotatePointAroundPivot(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z+ext.z), cen, go.transform.rotation)),
             //_camera.WorldToScreenPoint(RotatePointAroundPivot(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z+ext.z), cen, go.transform.rotation)),
             _camera.WorldToScreenPoint(RotatePointAroundPivot(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z-ext.z), cen, go.transform.rotation)),
             _camera.WorldToScreenPoint(RotatePointAroundPivot(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z-ext.z), cen, go.transform.rotation)),
             _camera.WorldToScreenPoint(RotatePointAroundPivot(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z+ext.z), cen, go.transform.rotation)),
             _camera.WorldToScreenPoint(RotatePointAroundPivot(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z+ext.z), cen, go.transform.rotation))
        };

        Vector2 min = extentPoints[0];
        Vector2 max = extentPoints[0];
        foreach (Vector2 v in extentPoints)
        {
            min = Vector2.Min(min, v);
            max = Vector2.Max(max, v);
        }

        rect = new Rect(min.x, Screen.height - min.y - (max.y - min.y), max.x - min.x, max.y - min.y);
        if (min.x < 0 || max.x > Screen.width || min.y < 0 || max.y > Screen.height)
        {
            return false;
        }

        //Debug
        //Vector3[] debugPoints = new Vector3[8]
        //{
        //     RotatePointAroundPivot(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z-ext.z), cen, go.transform.rotation),
        //     RotatePointAroundPivot(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z-ext.z), cen, go.transform.rotation),
        //     RotatePointAroundPivot(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z+ext.z), cen, go.transform.rotation),
        //     RotatePointAroundPivot(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z+ext.z), cen, go.transform.rotation),
        //     RotatePointAroundPivot(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z-ext.z), cen, go.transform.rotation),
        //     RotatePointAroundPivot(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z-ext.z), cen, go.transform.rotation),
        //     RotatePointAroundPivot(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z+ext.z), cen, go.transform.rotation),
        //     RotatePointAroundPivot(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z+ext.z), cen, go.transform.rotation)
        //};
        //DebugDrawCube(debugPoints, Color.cyan);
        DebugDrawRect(new Rect(min.x, min.y, max.x - min.x, max.y - min.y), Color.red);
        return true;
    }

    public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion rotation)
    {
        Vector3 dir = point - pivot; // get point direction relative to pivot
        dir = rotation * dir; // rotate it
        point = dir + pivot; // calculate rotated point
        return point; // return it
    }

    private void DebugDrawCube(Vector3[] cubeVerts, Color col)
    {
        int[] edges =  {0,1,
                        1,2,
                        2,3,
                        3,0,
                        4,5,
                        5,6,
                        6,7,
                        7,4,
                        1,5,
                        2,6,
                        3,7,
                        0,4};

        for (int i = 0; i < edges.Length; i += 2)
        {
            Debug.DrawLine(cubeVerts[edges[i]], cubeVerts[edges[i + 1]], col, _scenarioCooldown * 2.01f);
        }
    }

    private void DebugDrawRect(Rect rect, Color col)
    {
        Vector3 point0 = new Vector3(rect.xMin, rect.yMin, _camera.nearClipPlane + 0.01f);
        Vector3 point1 = new Vector3(rect.xMin, rect.yMax, _camera.nearClipPlane + 0.01f);
        Vector3 point2 = new Vector3(rect.xMax, rect.yMin, _camera.nearClipPlane + 0.01f);
        Vector3 point3 = new Vector3(rect.xMax, rect.yMax, _camera.nearClipPlane + 0.01f);
        Debug.DrawLine(_camera.ScreenToWorldPoint(point0), _camera.ScreenToWorldPoint(point1), col, _scenarioCooldown * 2.01f);
        Debug.DrawLine(_camera.ScreenToWorldPoint(point0), _camera.ScreenToWorldPoint(point2), col, _scenarioCooldown * 2.01f);
        Debug.DrawLine(_camera.ScreenToWorldPoint(point1), _camera.ScreenToWorldPoint(point3), col, _scenarioCooldown * 2.01f);
        Debug.DrawLine(_camera.ScreenToWorldPoint(point2), _camera.ScreenToWorldPoint(point3), col, _scenarioCooldown * 2.01f);
    }

    //private void OnGUI()
    //{
    //    foreach (var obj in _rects)
    //    {
    //        GUI.DrawTexture(new Rect(obj.Rect.x, obj.Rect.y, obj.Rect.size.x, obj.Rect.size.y), _rectDebugTexture);
    //    }
    //}
}
