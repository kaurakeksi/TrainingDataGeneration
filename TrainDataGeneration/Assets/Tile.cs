﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileValue
{
    m0,
    m1,
    m2,
    m3,
    m4,
    m5,
    m6,
    m7,
    m8,
    m9,
    p0,
    p1,
    p2,
    p3,
    p4,
    p5,
    p6,
    p7,
    p8,
    p9,
    s0,
    s1,
    s2,
    s3,
    s4,
    s5,
    s6,
    s7,
    s8,
    s9,
    z1,
    z2,
    z3,
    z4,
    z5,
    z6,
    z7,
    FaceDown,
    None
}

[System.Serializable]
public class TileData
{
    public TileValue Value;
    public List<Texture2D> Textures;

    public TileData(TileValue value, List<Texture2D> textures)
    {
        Textures = new List<Texture2D>();
        Value = value;
        Textures = textures;
    }
}

public class Tile : MonoBehaviour
{
    private TileValue _value;
    public TileValue Value
    {
        get
        {
            if (!_initialized)
            {
                return TileValue.None;
            }
            return _value;
        }
        set
        {
            _value = value;
        }
    }

    public bool Initialized
    {
        get { return _initialized; }
    }

    private Vector3 _center;
    private Vector3 _noRotationExtents;

    [SerializeField]
    private List<TileData> _tileDataList;

    private Material _material;
    private bool _initialized;

	private void Awake()
    {
        var renderer = GetComponent<MeshRenderer>();
        _material = renderer.material;
    }

    public void InitializeTile(bool faceDown)
    {
        if (!faceDown)
        {
            var data = _tileDataList[Random.Range(0, _tileDataList.Count)];
            bool dataHasTexture = false;
            while (!dataHasTexture)
            {
                if (data.Textures.Count > 0)
                {
                    dataHasTexture = true;
                }
                data = _tileDataList[Random.Range(0, _tileDataList.Count)];
            }

            // For the time being only use index 0 (requires some additional information on margin etc)
            var tempTexture = data.Textures[Random.Range(0, data.Textures.Count)];
            tempTexture.mipMapBias = -1.5f;
            _material.SetTexture("_FaceTex", tempTexture);
            _material.SetFloat("_Margin", Random.Range(0.05f, 0.1f));
            _value = data.Value;
        }
        else
        {
            _value = TileValue.FaceDown;
        }
        _initialized = true;
    }
}
